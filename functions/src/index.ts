import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as Firestore from "@google-cloud/firestore";

admin.initializeApp();

const RUNTIME_OPTIONS: functions.RuntimeOptions = {
  memory: "128MB",
  timeoutSeconds: 60,
};

export const joinToPark = functions
    .region("europe-west3")
    .firestore.document("/joinRequests/{requestID}")
    .onCreate((snapshot, context) => {
      const data = snapshot.data();
      if (data) {
        const userUid = data.userUid;
        const parkId = data.parkId;
        const p1 = snapshot.ref.delete();
        const p2 = admin
            .firestore()
            .doc(`parks/${parkId}`)
            .update("theboys", Firestore.FieldValue.arrayUnion(userUid));
        return Promise.all([p1, p2]);
      } else {
        return null;
      }
    });

export const sendNotification = functions
    .region("europe-west3")
    .runWith(RUNTIME_OPTIONS)
    .firestore.document("/parks/{parkId}/visits/{visitId}")
    .onWrite(async (change, context) => {
      const title =
        change.after.get("dog") +
        ": " +
        change.after.get("come") +
        " " +
        change.after.get("go");
      const body = "Tap to see it in the app!";
      const park = await change.after.ref.parent.parent?.get();
      const members: string[] = park?.get("theboys");
      const promises: Promise<any>[] = [];
      members.forEach((uid: string) => {
        const promise = admin
            .firestore()
            .doc(`users/${uid}`)
            .get()
            .then((snapshot) => {
              const fcmToken = snapshot.get("fcmtoken");
              const message: admin.messaging.Message = {
                notification: {
                  title: title,
                  body: body,
                },
                token: fcmToken,
              };
              return admin.messaging().send(message);
            })
            .catch((error) => {
              console.log(error);
            });
        promises.push(promise);
      });
      return Promise.all(promises);
    });
