package hu.bme.aut.android.dogparks.data

import java.io.Serializable

data class Dog(
    var name: String? = null,
    var type: String? = null,
    var weight: String? = null,
    var sex: String? = null,
    var imageUrl: String? = null,
    val time: String? = null,
    var theboys: List<String>? = null
) : Serializable {
    fun toHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            "name" to name,
            "type" to type,
            "weight" to weight,
            "sex" to sex,
            "imageUrl" to imageUrl,
            "time" to time,
            "theboys" to theboys,
        )
    }
}