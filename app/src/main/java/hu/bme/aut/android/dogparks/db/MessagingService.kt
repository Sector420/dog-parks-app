package hu.bme.aut.android.dogparks.db

import com.google.firebase.messaging.FirebaseMessagingService

class MessagingService : FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        DataBase.getInstance().sendRegistrationToServer(token)
    }
}