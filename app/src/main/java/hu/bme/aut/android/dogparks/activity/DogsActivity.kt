package hu.bme.aut.android.dogparks.activity

import android.app.ActivityOptions
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.dogparks.adapter.DogsAdapter
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.databinding.ActivityDogsBinding
import hu.bme.aut.android.dogparks.db.DataBase

class DogsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDogsBinding
    private lateinit var dogsAdapter: DogsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDogsBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        binding.rvDogs.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        dogsAdapter = DogsAdapter(this)

        binding.rvDogs.adapter = dogsAdapter

        initDogsListener()
    }

    private fun initDogsListener() {
        DataBase.getInstance().addDogListener { snapshots, e ->
            if (e != null) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
                return@addDogListener
            }

            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        dogsAdapter.addDog(dc.document.toObject<Dog>())
                    }
                    DocumentChange.Type.MODIFIED -> Toast.makeText(this, dc.document.data.toString(), Toast.LENGTH_SHORT).show()
                    DocumentChange.Type.REMOVED -> Toast.makeText(this, dc.document.data.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun startDogDetailsActivity(dog: Dog, imgDog: ImageView) {
        val options = ActivityOptions
            .makeSceneTransitionAnimation(this, imgDog, "sharedTransition")

        startActivity(DogDetailsActivity.createIntent(this, dog), options.toBundle())
    }
}