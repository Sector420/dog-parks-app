package hu.bme.aut.android.dogparks.data

import java.io.Serializable

data class Park(
    val author: String? = null,
    var address:  String? = null,
    var size: String? = null,
    var time: String? = null,
    var theboys: List<String>? = null
) : Serializable {
    fun toHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            "author" to author,
            "address" to address,
            "size" to size,
            "time" to time,
            "theboys" to theboys
        )
    }
}