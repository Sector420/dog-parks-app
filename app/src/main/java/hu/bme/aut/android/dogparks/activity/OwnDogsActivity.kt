package hu.bme.aut.android.dogparks.activity

import android.app.ActivityOptions
import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.dogparks.adapter.OwnDogsAdapter
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.databinding.ActivityOwnDogsBinding
import hu.bme.aut.android.dogparks.db.DataBase
import hu.bme.aut.android.dogparks.fragment.DogDialogFragment

class OwnDogsActivity : AppCompatActivity(), DogDialogFragment.DogHandler {
    private lateinit var binding: ActivityOwnDogsBinding
    private lateinit var dogsAdapter: OwnDogsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOwnDogsBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        dogsAdapter = OwnDogsAdapter(this)

        binding.rvDogs.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        binding.rvDogs.adapter = dogsAdapter

        binding.fab.setOnClickListener {
            DogDialogFragment().show(supportFragmentManager, "NEW_DOG")
        }

        initDogsListener()
    }

    private fun initDogsListener() {
        DataBase.getInstance().addOwnDogListener { snapshots, e ->
            if (e != null) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
                return@addOwnDogListener
            }

            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        dogsAdapter.addDog(
                            dc.document.toObject<Dog>(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.MODIFIED -> {
                        dogsAdapter.updateDog(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.REMOVED -> {
                        dogsAdapter.removeDog(dc.document.id)
                    }
                }
            }
        }
    }

    override fun createDog(newDog: Dog) {
        DataBase.getInstance().createDog(newDog)
    }

    override fun updateDog(dogId: String, dogToUpdate: Dog) {
        dogsAdapter.changeId(dogId)
        DataBase.getInstance().updateDog(dogId, dogToUpdate)
    }

    fun deleteDogDialog(dogId: String) {
        AlertDialog.Builder(this)
            .setMessage("Are you sure you want to delete this dog?")
            .setPositiveButton("Yes") { _, _ ->
                deleteDog(dogId)
            }
            .setNegativeButton("No", null)
            .show()
    }

    private fun deleteDog(dogId: String) {
        DataBase.getInstance().deleteDog(dogId)
    }

    fun editDog(dogId: String, dogToEdit: Dog) {
        val editDialog = DogDialogFragment()
        val bundle = Bundle()
        bundle.putString(DogDialogFragment.DOG_ID, dogId)
        bundle.putSerializable(DogDialogFragment.DOG_TO_EDIT, dogToEdit)
        editDialog.arguments = bundle
        editDialog.show(supportFragmentManager, "EDIT_DOG")
    }

    fun startDogDetailsActivity(dog: Dog, imgDog: ImageView) {
        val options = ActivityOptions
            .makeSceneTransitionAnimation(this, imgDog, "sharedTransition")

        startActivity(DogDetailsActivity.createIntent(this, dog), options.toBundle())
    }
}