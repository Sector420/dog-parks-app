package hu.bme.aut.android.dogparks.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import hu.bme.aut.android.dogparks.activity.OwnDogsActivity
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.databinding.CardOwnDogBinding

class OwnDogsAdapter (private val context: Context) :
    ListAdapter<Dog, OwnDogsAdapter.DogsViewHolder>(itemCallback) {

    private val dogList: MutableList<Dog> = mutableListOf()
    private val idMap: MutableMap<Dog, String> = mutableMapOf()
    private var lastPosition = -1

    class DogsViewHolder(binding: CardOwnDogBinding) : RecyclerView.ViewHolder(binding.root) {
        val tvName: TextView = binding.tvName
        val tvType: TextView = binding.tvType
        val tvWeight: TextView = binding.tvWeight
        val imgDog: ImageView = binding.imgDog
        val editButton = binding.EditButton
        val deleteButton = binding.RemoveButton
        val root = binding.root
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
       DogsViewHolder(
            CardOwnDogBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DogsViewHolder, position: Int) {
        val tmpDog = dogList[position]
        holder.tvName.text = tmpDog.name
        holder.tvType.text = tmpDog.type
        holder.tvWeight.text = tmpDog.weight

        if (tmpDog.imageUrl.isNullOrBlank()) {
            holder.imgDog.visibility = View.GONE
        } else {
            Glide.with(context).load(tmpDog.imageUrl).into(holder.imgDog)
            holder.imgDog.visibility = View.VISIBLE
        }
        holder.root.setOnClickListener{
            (context as OwnDogsActivity).startDogDetailsActivity(tmpDog, holder.imgDog)
        }
        holder.editButton.setOnClickListener {
            (context as OwnDogsActivity).editDog(idMap[tmpDog]!!, tmpDog)
        }
        holder.deleteButton.setOnClickListener {
            (context as OwnDogsActivity).deleteDogDialog(idMap[tmpDog]!!)
        }

        setAnimation(holder.itemView, position)
    }

    fun changeId(id : String) {
        for(park in dogList) {
            if(idMap[park] == null) {
                idMap[park] = id;
            }

        }
    }

    fun addDog(newDog: Dog?, id: String) {
        newDog ?: return
        idMap[newDog] = id
        dogList += newDog
        submitList(dogList)
        notifyItemInserted(dogList.lastIndex)
    }

    fun updateDog(modifiedDog: Dog?, dogId: String) {
        modifiedDog ?: return
        val index = dogList.indexOfFirst { dogList: Dog ->
            idMap[dogList].equals(dogId)
        }
        idMap.remove(dogList[index])
        dogList[index] = modifiedDog
        idMap[dogList[index]] = dogId
        submitList(dogList)
        notifyItemChanged(index)
    }

    fun removeDog(id: String) {
        val index = dogList.indexOfFirst { dogList: Dog ->
            idMap[dogList].equals(id)
        }
        idMap.remove(dogList[index])
        dogList.removeAt(index)
        submitList(dogList)
        notifyItemRemoved(index)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    companion object {
        object itemCallback : DiffUtil.ItemCallback<Dog>() {
            override fun areItemsTheSame(oldItem: Dog, newItem: Dog): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Dog, newItem: Dog): Boolean {
                return oldItem == newItem
            }
        }
    }
}