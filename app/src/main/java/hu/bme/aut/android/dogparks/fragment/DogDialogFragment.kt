package hu.bme.aut.android.dogparks.fragment

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import androidx.fragment.app.DialogFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import hu.bme.aut.android.dogparks.R
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.databinding.DialogCreateDogBinding
import hu.bme.aut.android.dogparks.extensions.validateNonEmpty
import java.io.ByteArrayOutputStream
import java.net.URLEncoder
import java.util.*
import kotlin.properties.Delegates

class DogDialogFragment : DialogFragment() {

    companion object {
        private const val REQUEST_CODE = 101
        const val DOG_ID = "PARK_ID"
        const val DOG_TO_EDIT = "PARK_TO_EDIT"
    }

    interface DogHandler {
        fun createDog(newDog: Dog)
        fun updateDog(dogId: String, dogToUpdate: Dog)
    }

    private var isEditDialog by Delegates.notNull<Boolean>()
    private lateinit var dogHandler: DogHandler

    private lateinit var etName: EditText
    private lateinit var etType: EditText
    private lateinit var etWeight: EditText
    private lateinit var sex: Spinner
    private lateinit var imgAttach: ImageView

    private lateinit var binding: DialogCreateDogBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context !is DogHandler) {
            throw RuntimeException("The Activity does not implement the interface")
        }
        dogHandler = context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        isEditDialog = (arguments != null && arguments!!.containsKey(DOG_TO_EDIT))
        binding = DialogCreateDogBinding.inflate(LayoutInflater.from(context))
        etName = binding.root.findViewById<EditText>(R.id.etName)
        etType = binding.root.findViewById<EditText>(R.id.etType)
        etWeight = binding.root.findViewById<EditText>(R.id.etWeight)
        sex = binding.root.findViewById<Spinner>(R.id.Sex)
        sex.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            resources.getStringArray(R.array.sex)
        )
        imgAttach = binding.root.findViewById<ImageView>(R.id.imgAttach)
        binding.btnAttach.setOnClickListener { attachClick() }
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setTitle(
                    when (isEditDialog) {
                        true -> "Edit Dog"
                        false -> "Add Dog"
                    }
                )
                .setView(binding.root)
                .setPositiveButton(
                    when (isEditDialog) {
                        true -> "Edit"
                        false -> "Create"
                    }, null
                )
                .setNegativeButton("Cancel") { _, _ -> dialog?.cancel() }
            if (isEditDialog) {
                val dogToEdit =
                    arguments!!.getSerializable(DOG_TO_EDIT) as Dog
                etName.setText(dogToEdit.name)
                etType.setText(dogToEdit.type)
                etWeight.setText(dogToEdit.weight)
            }
            builder.create()
        } ?: throw IllegalStateException("Fragment cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (validateForm()) {
                when (isEditDialog) {
                    true -> {
                        if (binding.imgAttach.visibility != View.VISIBLE) {
                            handleDogEdit()
                        }
                        else {
                            uploadPostWithImage()
                        }
                    }
                    false -> {
                        if (binding.imgAttach.visibility != View.VISIBLE) {
                            handleDogCreate()
                        }
                        else {
                            uploadPostWithImage()
                        }
                    }
                }
                dialog.dismiss()
            }
        }
    }

    private fun validateForm() =
        etName.validateNonEmpty() && etType.validateNonEmpty() && etWeight.validateNonEmpty()

    private fun attachClick() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(takePictureIntent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }

        if (requestCode == REQUEST_CODE) {
            val imageBitmap = data?.extras?.get("data") as? Bitmap ?: return
            binding.imgAttach.setImageBitmap(imageBitmap)
            binding.imgAttach.visibility = View.VISIBLE
        }
    }

    private fun uploadPostWithImage() {
        val bitmap: Bitmap = (binding.imgAttach.drawable as BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val imageInBytes = baos.toByteArray()

        val storageReference = FirebaseStorage.getInstance().reference
        val newImageName = URLEncoder.encode(UUID.randomUUID().toString(), "UTF-8") + ".jpg"
        val newImageRef = storageReference.child("images/$newImageName")

        newImageRef.putBytes(imageInBytes)
            .continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let { throw it }
                }

                newImageRef.downloadUrl
            }
            .addOnSuccessListener { downloadUri ->
                when (isEditDialog) {
                    true -> handleDogEdit(downloadUri.toString())
                    false -> handleDogCreate(downloadUri.toString())
                }
            }
    }

    private fun handleDogCreate(imageUrl: String? = null) {
        val uid = FirebaseAuth.getInstance().currentUser?.uid
        val currentTime: Date = Calendar.getInstance().time
        val newDog = Dog(
            binding.etName.text.toString(),
            binding.etType.text.toString(),
            binding.etWeight.text.toString() + " kg",
            sex.selectedItem.toString(),
            imageUrl,
            currentTime.toString(),
            mutableListOf(uid!!),
        )
        dogHandler.createDog(newDog)
    }

    private fun handleDogEdit(imageUrl: String? = null) {
        val dogToEdit = arguments!!.getSerializable(DOG_TO_EDIT) as Dog
        val dogId = arguments!!.getString(DOG_ID)!!
        dogToEdit.name = etName.text.toString()
        dogToEdit.type = etType.text.toString()
        dogToEdit.weight = etWeight.text.toString()
        dogToEdit.sex = sex.selectedItem.toString()
        if(imageUrl != null) {
            dogToEdit.imageUrl = imageUrl
        }
        dogHandler.updateDog(dogId, dogToEdit)
    }
}