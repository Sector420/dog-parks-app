package hu.bme.aut.android.dogparks.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.dogparks.R
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.data.Visit
import hu.bme.aut.android.dogparks.databinding.DialogCreateVisitBinding
import hu.bme.aut.android.dogparks.extensions.validateNonEmpty
import java.util.*
import kotlin.properties.Delegates

class VisitDialogFragment(dogList: MutableList<Dog>) : DialogFragment() {

    interface VisitHandler {
        fun createVisit(newVisit: Visit)
        fun updateVisit(visitId: String, visitToUpdate: Visit)
    }

    companion object {
        const val VISIT_ID = "VISIT_ID"
        const val VISIT_TO_EDIT = "VISIT_TO_EDIT"
    }

    private var allDog = dogList

    private var isEditDialog by Delegates.notNull<Boolean>()
    private lateinit var visitHandler: VisitHandler

    private lateinit var etCome: EditText
    private lateinit var etGo: EditText
    private lateinit var dogs: Spinner

    private lateinit var binding: DialogCreateVisitBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context !is VisitHandler) {
            throw RuntimeException("The Activity does not implement the interface")
        }
        visitHandler = context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        isEditDialog = (arguments != null && arguments!!.containsKey(VISIT_TO_EDIT))
        binding = DialogCreateVisitBinding.inflate(LayoutInflater.from(context))
        etCome = binding.root.findViewById<EditText>(R.id.etCome)
        etGo = binding.root.findViewById<EditText>(R.id.etGo)
        dogs = binding.root.findViewById<Spinner>(R.id.Dogs)
        val names: MutableList<String?> = mutableListOf()
        for(dog in allDog) {
            names.add(dog.name)
        }
        dogs.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            names
        )
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setTitle(
                    when (isEditDialog) {
                        true -> "Edit Visit"
                        false -> "New Visit"
                    }
                )
                .setView(binding.root)
                .setPositiveButton(
                    when (isEditDialog) {
                        true -> "Edit"
                        false -> "Create"
                    }, null
                )
                .setNegativeButton("Cancel") { _, _ -> dialog?.cancel() }
            if (isEditDialog) {
                val visitToEdit =
                    arguments!!.getSerializable(VISIT_TO_EDIT) as Visit
                etCome.setText(visitToEdit.come)
                etGo.setText(visitToEdit.go)
            }
            builder.create()
        } ?: throw IllegalStateException("Fragment cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (validateForm()) {
                when (isEditDialog) {
                    true -> handleVisitEdit()
                    false -> handleVisitCreate()
                }
                dialog.dismiss()
            }
        }
    }

    private fun validateForm() = etCome.validateNonEmpty() && etGo.validateNonEmpty()

    private fun handleVisitCreate() {
        val currentTime: Date = Calendar.getInstance().time
        val newVisit = Visit(dogs.selectedItem.toString(),etCome.text.toString(),etGo.text.toString(), currentTime.toString())
        visitHandler.createVisit(newVisit)
    }

    private fun handleVisitEdit() {
        val visitToEdit = arguments!!.getSerializable(VISIT_TO_EDIT) as Visit
        val visitId = arguments!!.getString(VISIT_ID)!!
        visitToEdit.dog = dogs.selectedItem.toString()
        visitToEdit.come = etCome.text.toString()
        visitToEdit.go = etGo.text.toString()
        visitHandler.updateVisit(visitId, visitToEdit)
    }
}