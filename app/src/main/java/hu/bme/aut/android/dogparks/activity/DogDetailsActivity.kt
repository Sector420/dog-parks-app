package hu.bme.aut.android.dogparks.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import com.bumptech.glide.Glide
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.databinding.ActivityDogDetailsBinding

class DogDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDogDetailsBinding

    companion object {
        private const val TAG = "DogDetalisActivity"

        fun createIntent(context: Context, dog: Dog): Intent {
            val extras = Bundle()
            extras.putSerializable("DOG", dog)
            return Intent().setClass(context, DogDetailsActivity::class.java).putExtras(extras)
        }
    }

    private lateinit var dog: Dog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDogDetailsBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        dog = intent.getSerializableExtra("DOG") as Dog

        Log.d("DOG", dog.toString())

        binding.tvName.text = dog.name
        binding.tvType.text = dog.type
        binding.tvWeight.text = dog.weight
        binding.tvSex.text = dog.sex
        Glide.with(this).load(dog.imageUrl).into(binding.ivDogImage)
    }
}