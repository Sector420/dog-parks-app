package hu.bme.aut.android.dogparks.data

import java.io.Serializable

data class Visit(
    var dog: String? = null,
    var come: String? = null,
    var go: String? = null,
    val time: String? = null,
) : Serializable {
    fun toHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            "dog" to dog,
            "come" to come,
            "go" to go,
            "time" to time
        )
    }
}