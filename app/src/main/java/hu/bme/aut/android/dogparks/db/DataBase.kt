package hu.bme.aut.android.dogparks.db

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.data.Park
import hu.bme.aut.android.dogparks.data.Visit
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class DataBase {
    companion object {
        private const val TAG = "DataBase"

        private const val USERS = "users"
        private const val FCM_TOKEN = "fcmToken"
        private const val PARKS = "parks"
        private const val DOGS = "dogs"
        private const val VISITS = "visits"
        private const val THEBOYS = "theboys"
        private const val USER_UID = "userUid"
        private const val PARK_ID = "parkId"
        private const val JOIN_REQUESTS = "joinRequests"

        private var instance = DataBase()

        fun getInstance(): DataBase {
            return instance
        }
    }

    private var db = Firebase.firestore

    fun createPark(newPark: Park) {
        db.collection(PARKS)
            .add(newPark)
    }

    fun updatePark(parkId: String, parkToUpdate: Park) {
        db.collection(PARKS).document(parkId)
            .update(parkToUpdate.toHashMap())
    }

    fun deletePark(parkId: String) {
        db.collection(PARKS).document(parkId).delete()
    }

    @DelicateCoroutinesApi
    fun deleteMember(parkId: String, uid: String?) {
        var park: Park
        GlobalScope.launch(Dispatchers.IO) {
            park = db.collection(PARKS).document(parkId).get().await().toObject(Park::class.java)!!
            val index = park.theboys!!.indexOfFirst { it ->
                it == uid
            }
            val members: MutableList<Park> = park.theboys as MutableList<Park>
            members.removeAt(index)
            db.collection(PARKS).document(parkId).update("theboys",members )
        }
    }

    fun addParkListener(listener: EventListener<QuerySnapshot?>) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        db.collection(PARKS).whereArrayContains(THEBOYS, user.uid)
            .addSnapshotListener(listener)
    }

    fun createDog(newDog: Dog) {
        db.collection(DOGS)
            .add(newDog)
    }

    fun deleteDog(dogId: String) {
        db.collection(DOGS).document(dogId).delete()
    }

    fun updateDog(dogId: String, dogToUpdate: Dog) {
        db.collection(DOGS).document(dogId)
            .update(dogToUpdate.toHashMap())
    }

    fun getOwnDogs() : Query {
        val user = FirebaseAuth.getInstance().currentUser
        return db.collection(DOGS).whereArrayContains(THEBOYS, user!!.uid)
    }

    fun addDogListener(listener: EventListener<QuerySnapshot?>) {
        db.collection(DOGS)
            .addSnapshotListener(listener)
    }

    fun addOwnDogListener(listener: EventListener<QuerySnapshot?>) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        db.collection(DOGS).whereArrayContains(THEBOYS, user.uid)
            .addSnapshotListener(listener)
    }

    fun createVisit(parkId: String, newVisit: Visit) {
        db.collection(PARKS).document(parkId).collection(VISITS)
            .add(newVisit)
    }

    fun updateVisit(parkId: String, visitId: String, visitToUpdate: Visit) {
        db.collection(PARKS).document(parkId).collection(VISITS)
            .document(visitId)
            .update(visitToUpdate.toHashMap())
    }

    fun deleteVisit(parkId: String, visitId: String) {
        db.collection(PARKS).document(parkId).collection(VISITS).document(visitId)
            .delete()
    }

    fun addVisitListener(parkId: String, listener: EventListener<QuerySnapshot?>) {
        db.collection(PARKS).document(parkId).collection(VISITS)
            .addSnapshotListener(listener)
    }

    fun joinPark(parkId: String) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        val joinRequest = hashMapOf<String, Any>(
            USER_UID to user.uid,
            PARK_ID to parkId
        )
        db.collection(JOIN_REQUESTS)
            .add(joinRequest)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "Join request added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { error ->
                Log.w(TAG, "Error adding join request", error)
            }
    }

    fun sendRegistrationToServer(token: String) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        db.collection(USERS).document(user.uid).set(hashMapOf(FCM_TOKEN to token))
    }
}
