package hu.bme.aut.android.dogparks.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.android.dogparks.activity.VisitsActivity
import hu.bme.aut.android.dogparks.data.Visit
import hu.bme.aut.android.dogparks.databinding.CardVisitBinding

class VisitsAdapter(private val context: Context) :
    ListAdapter<Visit, VisitsAdapter.VisitViewHolder>(itemCallback) {

    private var visitList: MutableList<Visit> = mutableListOf()
    private val idMap: MutableMap<Visit, String> = mutableMapOf()
    private var lastPosition = -1

    class VisitViewHolder(binding: CardVisitBinding) : RecyclerView.ViewHolder(binding.root) {
        val tvDog: TextView = binding.tvDog
        val tvTime: TextView = binding.tvTime
        val editButton = binding.EditButton
        val deleteButton = binding.RemoveButton
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        VisitViewHolder(CardVisitBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VisitViewHolder, position: Int) {
        val tmpVisit = visitList[position]
        holder.tvDog.text = tmpVisit.dog
        holder.tvTime.text = "${tmpVisit.come} - ${tmpVisit.go}"
        holder.editButton.setOnClickListener {
            (context as VisitsActivity).editVisit(idMap[tmpVisit]!!, tmpVisit)
        }
        holder.deleteButton.setOnClickListener {
            (context as VisitsActivity).deleteVisitDialog(idMap[tmpVisit]!!)
        }
        setAnimation(holder.itemView, position)
    }

    fun changeId(id : String) {
        for(park in visitList) {
            if(idMap[park] == null) {
                idMap[park] = id;
            }

        }
    }

    fun addVisit(newVisit: Visit?, id: String) {
        newVisit ?: return
        idMap[newVisit] = id
        visitList += newVisit
        submitList(visitList)
        notifyItemInserted(visitList.lastIndex)
    }

    fun updateVisit(modifiedPark: Visit?, visitId: String) {
        modifiedPark ?: return
        val index = visitList.indexOfFirst { visitList: Visit ->
            idMap[visitList].equals(visitId)
        }
        idMap.remove(visitList[index])
        visitList[index] = modifiedPark
        idMap[visitList[index]] = visitId
        submitList(visitList)
        notifyItemChanged(index)
    }

    fun removeVisit(id: String) {
        val index = visitList.indexOfFirst { visitList: Visit ->
            idMap[visitList].equals(id)
        }
        idMap.remove(visitList[index])
        visitList.removeAt(index)
        submitList(visitList)
        notifyItemRemoved(index)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    companion object {
        object itemCallback : DiffUtil.ItemCallback<Visit>() {
            override fun areItemsTheSame(oldItem: Visit, newItem: Visit): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Visit, newItem: Visit): Boolean {
                return oldItem == newItem
            }
        }
    }
}