package hu.bme.aut.android.dogparks.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.dogparks.R

class JoinDialogFragment : DialogFragment() {

    interface JoinHandler {
        fun join(code: String)
    }

    private lateinit var joinHandler: JoinHandler

    private lateinit var etCode: EditText

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context !is JoinHandler) {
            throw RuntimeException("The Activity does not implement the JoinHandler interface")
        }
        joinHandler = context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val rootView = inflater.inflate(R.layout.dialog_join, null)
            builder
                .setTitle("Join with the code")
                .setView(rootView)
                .setPositiveButton("Join", null)
                .setNegativeButton("Cancel") { _, _ -> dialog?.cancel() }
            etCode = rootView.findViewById(R.id.etCode)
            builder.create()
        } ?: throw IllegalStateException("Fragment cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            val code = etCode.text.toString()
            joinHandler.join(code)
            dialog.dismiss()
        }
    }
}
