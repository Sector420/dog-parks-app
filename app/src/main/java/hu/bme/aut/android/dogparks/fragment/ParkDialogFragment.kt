package hu.bme.aut.android.dogparks.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.google.firebase.auth.FirebaseAuth
import hu.bme.aut.android.dogparks.R
import hu.bme.aut.android.dogparks.data.Park
import hu.bme.aut.android.dogparks.databinding.DialogCreateParkBinding
import hu.bme.aut.android.dogparks.extensions.validateNonEmpty
import java.util.*
import kotlin.properties.Delegates

class ParkDialogFragment() : DialogFragment() {

    interface ParkHandler {
        fun createPark(newPark: Park)
        fun updatePark(parkId: String, parkToUpdate: Park)
    }

    companion object {
        const val PARK_ID = "PARK_ID"
        const val PARK_TO_EDIT = "PARK_TO_EDIT"
    }

    private var isEditDialog by Delegates.notNull<Boolean>()
    private lateinit var parkHandler: ParkHandler

    private lateinit var etAddress: EditText
    private lateinit var etSize: EditText

    private lateinit var binding: DialogCreateParkBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context !is ParkHandler) {
            throw RuntimeException("The Activity does not implement the interface")
        }
        parkHandler = context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        isEditDialog = (arguments != null && arguments!!.containsKey(PARK_TO_EDIT))
        binding = DialogCreateParkBinding.inflate(LayoutInflater.from(context))
        etAddress = binding.root.findViewById<EditText>(R.id.etAddress)
        etSize = binding.root.findViewById<EditText>(R.id.etSize)
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder
                .setTitle(
                    when (isEditDialog) {
                        true -> "Edit Park"
                        false -> "Add park"
                    }
                )
                .setView(binding.root)
                .setPositiveButton(
                    when (isEditDialog) {
                        true -> "Edit"
                        false -> "Create"
                    }, null
                )
                .setNegativeButton("Cancel") { _, _ -> dialog?.cancel() }
            if (isEditDialog) {
                val parkToEdit =
                    arguments!!.getSerializable(PARK_TO_EDIT) as Park
                etAddress.setText(parkToEdit.address)
                etSize.setText(parkToEdit.size)
            }
            builder.create()
        } ?: throw IllegalStateException("Fragment cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (validateForm()) {
                when (isEditDialog) {
                    true -> handleParkEdit()
                    false -> handleParkCreate()
                }
                dialog.dismiss()
            }
        }
    }

    private fun validateForm() = etAddress.validateNonEmpty() && etSize.validateNonEmpty()

    private fun handleParkCreate() {
        val uid = FirebaseAuth.getInstance().currentUser?.uid
        val currentTime: Date = Calendar.getInstance().time
        val newPark = Park(uid, etAddress.text.toString(), etSize.text.toString() + " square meter", currentTime.toString(), mutableListOf(uid!!))
        parkHandler.createPark(newPark)
    }

    private fun handleParkEdit() {
        val parkToEdit = arguments!!.getSerializable(PARK_TO_EDIT) as Park
        val shoppingListId = arguments!!.getString(PARK_ID)!!
        parkToEdit.address = etAddress.text.toString()
        parkToEdit.size = etSize.text.toString()
        parkHandler.updatePark(shoppingListId, parkToEdit)
    }

}