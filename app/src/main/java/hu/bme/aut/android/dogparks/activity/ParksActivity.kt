package hu.bme.aut.android.dogparks.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.Toast
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.dogparks.R
import hu.bme.aut.android.dogparks.adapter.ParksAdapter
import hu.bme.aut.android.dogparks.data.Park
import hu.bme.aut.android.dogparks.databinding.ActivityParksBinding
import hu.bme.aut.android.dogparks.databinding.NavHeaderParksBinding
import hu.bme.aut.android.dogparks.db.DataBase
import hu.bme.aut.android.dogparks.fragment.JoinDialogFragment
import hu.bme.aut.android.dogparks.fragment.ParkDialogFragment
import kotlinx.coroutines.DelicateCoroutinesApi


class ParksActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, ParkDialogFragment.ParkHandler, JoinDialogFragment.JoinHandler {

    private lateinit var bindingNavHeader : NavHeaderParksBinding
    private lateinit var binding: ActivityParksBinding
    private lateinit var parksAdapter: ParksAdapter
    val user = FirebaseAuth.getInstance().currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParksBinding.inflate(LayoutInflater.from(this))
        bindingNavHeader = NavHeaderParksBinding.inflate(layoutInflater)
        bindingNavHeader.textView.text = user?.email
        setContentView(binding.root)
        setSupportActionBar(binding.appBarParks.toolbar)

        binding.appBarParks.fab.setOnClickListener { view ->
            ParkDialogFragment().show(supportFragmentManager, "NEW_PARK")
        }
        binding.navView.setNavigationItemSelectedListener(this)

        parksAdapter = ParksAdapter(this)

        binding.appBarParks.contentParks.rvPosts.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        binding.appBarParks.contentParks.rvPosts.adapter = parksAdapter

        initParksListener();
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_dogs -> {
                startActivity(Intent(this, DogsActivity::class.java))
            }
            R.id.nav_own_dogs -> {
                startActivity(Intent(this, OwnDogsActivity::class.java))
            }
            R.id.nav_join -> {
                joinWithCode()
            }
            R.id.nav_logout -> {
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun initParksListener() {
        DataBase.getInstance().addParkListener { snapshots, e ->
            if (e != null) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
                return@addParkListener
            }

            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        parksAdapter.addPark(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.MODIFIED -> {
                        parksAdapter.updatePark(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.REMOVED -> {
                        parksAdapter.removePark(dc.document.id)
                    }
                }
            }
        }
    }

    override fun createPark(newPark: Park) {
        DataBase.getInstance().createPark(newPark)
    }

    override fun updatePark(parkId: String, parkToUpdate: Park) {
        parksAdapter.changeId(parkId)
        DataBase.getInstance().updatePark(parkId, parkToUpdate)
    }

    @DelicateCoroutinesApi
    fun deleteParkDialog(parkId: String, tmpPark: Park) {
        val isYouAuthor: Boolean = tmpPark.author == user?.uid
        val title: String = if(isYouAuthor) {
            "Are you sure you want to delete this park?"
        } else {
            "Are you sure you want to leave this park?"
        }
        AlertDialog.Builder(this)
            .setMessage(title)
            .setPositiveButton("Yes") { _, _ ->
                yesButtonClicked(parkId, isYouAuthor, user?.uid)
            }
            .setNegativeButton("No", null)
            .show()
    }

    @DelicateCoroutinesApi
    private fun yesButtonClicked(parkId: String, isYouAuthor: Boolean, uid: String?) {
        if(isYouAuthor) {
            deletePark(parkId)
        }
        else {
            DataBase.getInstance().deleteMember(parkId, uid)
        }
    }

    private fun deletePark(parkId: String) {
        DataBase.getInstance().deletePark(parkId)
    }

    fun editPark(parkId: String, parkToEdit: Park) {
        val editDialog = ParkDialogFragment()
        val bundle = Bundle()
        bundle.putString(ParkDialogFragment.PARK_ID, parkId)
        bundle.putSerializable(ParkDialogFragment.PARK_TO_EDIT, parkToEdit)
        editDialog.arguments = bundle
        editDialog.show(supportFragmentManager, "EDIT_PARK")
    }


    fun startVisitActivity(parkId: String) {
        startActivity(VisitsActivity.createIntent(this, parkId))
    }

    fun sharePark(parkId: String) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, parkId)
        shareIntent.type = "text/plain"
        startActivity(Intent.createChooser(shareIntent, "Share date list"))
    }

    private fun joinWithCode() {
        JoinDialogFragment().show(supportFragmentManager, "null")
    }

    override fun join(code: String) {
        DataBase.getInstance().joinPark(code)
    }

}