package hu.bme.aut.android.dogparks.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.android.dogparks.activity.ParksActivity
import hu.bme.aut.android.dogparks.data.Park
import hu.bme.aut.android.dogparks.databinding.CardParkBinding
import kotlinx.coroutines.DelicateCoroutinesApi

class ParksAdapter(private val context: Context) :
    ListAdapter<Park, ParksAdapter.ParkViewHolder>(itemCallback) {

    private var parkList: MutableList<Park> = mutableListOf()
    private val idMap: MutableMap<Park, String> = mutableMapOf()
    private var lastPosition = -1

    class ParkViewHolder(binding: CardParkBinding) : RecyclerView.ViewHolder(binding.root) {
        val tvAddress: TextView = binding.tvAddress
        val tvSize: TextView = binding.tvSize
        val editButton = binding.EditButton
        val shareButton = binding.ShareButton
        val deleteButton = binding.RemoveButton
        val root = binding.root
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ParkViewHolder(CardParkBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    @DelicateCoroutinesApi
    override fun onBindViewHolder(holder: ParkViewHolder, position: Int) {
        val tmpPark = parkList[position]
        val tmpParkId = idMap[tmpPark]!!
        holder.tvAddress.text = tmpPark.address
        holder.tvSize.text = tmpPark.size

        holder.root.setOnClickListener {
            (context as ParksActivity).startVisitActivity(idMap[tmpPark]!!)
        }
        holder.editButton.setOnClickListener {
            (context as ParksActivity).editPark(
                tmpParkId,
                tmpPark
            )
        }
        holder.shareButton.setOnClickListener {
            (context as ParksActivity).sharePark(idMap[tmpPark]!!)
        }
        holder.deleteButton.setOnClickListener {
            (context as ParksActivity).deleteParkDialog(idMap[tmpPark]!!, tmpPark)
        }
        setAnimation(holder.itemView, position)
    }

    fun changeId(id : String) {
        for(park in parkList) {
            if(idMap[park] == null) {
                idMap[park] = id;
            }

        }
    }

    fun addPark(newPark: Park?, id: String) {
        newPark ?: return
        idMap[newPark] = id
        parkList += newPark
        submitList(parkList)
        notifyItemInserted(parkList.lastIndex)
    }

    fun updatePark(modifiedPark: Park?, parkId: String) {
        modifiedPark ?: return
        val index = parkList.indexOfFirst { parkList: Park ->
            idMap[parkList].equals(parkId)
        }
        idMap.remove(parkList[index])
        parkList[index] = modifiedPark
        idMap[parkList[index]] = parkId
        submitList(parkList)
        notifyItemChanged(index)
    }

    fun removePark(parkId: String) {
        val index = parkList.indexOfFirst { parkList: Park ->
            idMap[parkList].equals(parkId)
        }
        idMap.remove(parkList[index])
        parkList.removeAt(index)
        submitList(parkList)
        notifyItemRemoved(index)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    companion object {

        private const val TAG = "ParkAdapter"

        object itemCallback : DiffUtil.ItemCallback<Park>() {
            override fun areItemsTheSame(oldItem: Park, newItem: Park): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Park, newItem: Park): Boolean {
                return oldItem == newItem
            }
        }
    }
}