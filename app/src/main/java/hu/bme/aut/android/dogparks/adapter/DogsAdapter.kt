package hu.bme.aut.android.dogparks.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import hu.bme.aut.android.dogparks.activity.DogsActivity
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.databinding.CardDogBinding


class DogsAdapter (private val context: Context) :
    ListAdapter<Dog, DogsAdapter.DogsViewHolder>(itemCallback) {

    private val dogList: MutableList<Dog> = mutableListOf()
    private var lastPosition = -1

    class DogsViewHolder(binding: CardDogBinding) : RecyclerView.ViewHolder(binding.root) {
        val tvName: TextView = binding.tvName
        val tvType: TextView = binding.tvType
        val tvWeight: TextView = binding.tvWeight
        val imgDog: ImageView = binding.imgDog
        val root = binding.root
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DogsViewHolder(
            CardDogBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DogsViewHolder, position: Int) {
        val tmpDog = dogList[position]
        holder.tvName.text = tmpDog.name
        holder.tvType.text = tmpDog.type
        holder.tvWeight.text = tmpDog.weight

        if (tmpDog.imageUrl.isNullOrBlank()) {
            holder.imgDog.visibility = View.GONE
        } else {
            Glide.with(context).load(tmpDog.imageUrl).into(holder.imgDog)
            holder.imgDog.visibility = View.VISIBLE
        }

        holder.root.setOnClickListener{
            (context as DogsActivity).startDogDetailsActivity(tmpDog, holder.imgDog)
        }

        setAnimation(holder.itemView, position)
    }

    fun addDog(newDog: Dog?) {
        newDog ?: return
        dogList += newDog
        submitList(dogList)
        notifyItemInserted(dogList.lastIndex)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    companion object {
        object itemCallback : DiffUtil.ItemCallback<Dog>() {
            override fun areItemsTheSame(oldItem: Dog, newItem: Dog): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Dog, newItem: Dog): Boolean {
                return oldItem == newItem
            }
        }
    }
}