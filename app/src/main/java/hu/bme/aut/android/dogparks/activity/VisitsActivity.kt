package hu.bme.aut.android.dogparks.activity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.dogparks.adapter.VisitsAdapter
import hu.bme.aut.android.dogparks.data.Dog
import hu.bme.aut.android.dogparks.data.Visit
import hu.bme.aut.android.dogparks.databinding.ActivityVisitsBinding
import hu.bme.aut.android.dogparks.db.DataBase
import hu.bme.aut.android.dogparks.fragment.VisitDialogFragment
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class VisitsActivity : AppCompatActivity(), VisitDialogFragment.VisitHandler {

    companion object {
        private const val TAG = "CostsActivity"

        fun createIntent(context: Context, id: String): Intent {
            val extras = Bundle()
            extras.putString("PARK_ID", id)
            return Intent().setClass(context, VisitsActivity::class.java).putExtras(extras)
        }
    }
    private lateinit var binding: ActivityVisitsBinding
    private lateinit var visitsAdapter: VisitsAdapter
    private lateinit var parkId: String
    private var realdogs: MutableList<Dog> = mutableListOf()

    @DelicateCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVisitsBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        GlobalScope.launch(Dispatchers.IO) {
            realdogs = DataBase.getInstance().getOwnDogs().get().await().toObjects(Dog::class.java)
        }
        parkId = intent.getStringExtra("PARK_ID")!!

        visitsAdapter = VisitsAdapter(this)

        binding.rvVisits.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        binding.rvVisits.adapter = visitsAdapter

        binding.fab.setOnClickListener {
            VisitDialogFragment(realdogs).show(supportFragmentManager, "NEW_VISIT")
        }

        initVisitsListener()
    }

    private fun initVisitsListener() {
        DataBase.getInstance().addVisitListener(parkId) { snapshots, e ->
            if (e != null) {
                Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
                return@addVisitListener
            }

            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        visitsAdapter.addVisit(
                            dc.document.toObject<Visit>(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.MODIFIED -> {
                        visitsAdapter.updateVisit(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.REMOVED -> {
                        visitsAdapter.removeVisit(dc.document.id)
                    }
                }
            }
        }
    }

    fun editVisit(visitId: String, visitiToEdit: Visit) {
        val editDialog = VisitDialogFragment(realdogs)
        val bundle = Bundle()
        bundle.putString(VisitDialogFragment.VISIT_ID, visitId)
        bundle.putSerializable(VisitDialogFragment.VISIT_TO_EDIT, visitiToEdit)
        editDialog.arguments = bundle
        editDialog.show(supportFragmentManager, "EDIT_VISIT")
    }

    override fun createVisit(newVisit: Visit) {
        DataBase.getInstance().createVisit(parkId, newVisit)
    }

    override fun updateVisit(visitId: String, visitToUpdate: Visit) {
        visitsAdapter.changeId(visitId)
        DataBase.getInstance().updateVisit(parkId, visitId, visitToUpdate)
    }

    fun deleteVisitDialog(visitId: String) {
        AlertDialog.Builder(this)
            .setMessage("Are you sure you want to delete this visit?")
            .setPositiveButton("Yes") { _, _ ->
                deleteVisit(visitId)
            }
            .setNegativeButton("No", null)
            .show()
    }

    private fun deleteVisit(visitID: String) {
        DataBase.getInstance().deleteVisit(parkId, visitID)
    }
}